
# Motion capture measurement requirements
* Stick at least 3 markers on a single rigid body. The markers that belong to the same rigid body are referred to as a _cluster_.
* Make sure you have an object that has a tip that can be used to point at locations on other rigid bodies. This object is referred to as the _probe_. The probe must be a single rigid body and have at least 3 markers on it (i.e. a probe cluster).
* Perform a short measurement (~10 sec) where you use the probe tip to point at a location relative to cluster of another rigid body. Make sure the clusters of both the probe and the rigid body pointed at are visible. These measurements are referred to as a _probe trials_. The location pointed at with the probe tip is referred to as a _point_. A single probe trial must be done for each desired point.
* Perform other measurements with only the rigid body clusters visible, and not (necessarily) the probe cluster. These measurements are referred to as _measurement trials_.
* The goal is to reconstruct those locations in the measurement trials that were pointed at with the probe during the probe trials. 
* A hard requirement is that the same markers accross all probe trials and all measurement trials have the same name (label).

# Reconstruction process
* It must be known what the local location of the probe markers are relative to the tip. The tip is assumed to be in 0, 0, 0 (x, y z).
* In the probe trial, determine the global location of the probe tip (i.e. the point) based on the global location of the probe cluster. This can be done using singular value decomposition to estimate the rotation matrix and translation vector from local probe coordinates to global probe coordinates. 
* In the probe trial, determine the local location of the rigid body cluster relative to the point.
* In each measurement trial, determine the global location of the point based on the global location of the rigid body cluster. Again, this can be done using singular value decomposition.

# Running the code
See main.py and the README.md in the *settings* folder.

## Assumed data format
The BatchReconstruct class in batch.py makes assumptions on the data reader and data format. It depends on py_readc3d.

# To Do
* Implement a warning or error if no matching names found and reconstruction cannot be done. Currently will throw invalid divide and give NaNs
* There's currently no way to build 1 settings file for multiple subjects. Don't think you should, either, but it makes the list formatting obsolete in the settings file of trials.