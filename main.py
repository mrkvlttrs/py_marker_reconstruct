
import json
from reconstruction.batch import BatchReconstruct

settings = json.load(open('./settings/settings.json'))
reconstructor = BatchReconstruct(settings)
reconstructor.run()

# The reconstructed data per trial can be found in reconstructor.data_per_trial
