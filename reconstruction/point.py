"""
Class to do marker cluster related processing for point reconstruction
"""

import numpy as np
from spatial.euler_angle_estimate import r_svd


class Point(object):
    """
    A cluster is a set of markers on a rigid body. Relative to a cluster we
    can define another point on the same rigid body, that moves along
    with the cluster.
    """

    def __init__(self, point_settings, marker_local=None):
        """
        =INPUT=
            point_name - string
                Name of the point to be reconstructed from the cluster
            cluster_name - string
                Name of the cluster to which the point belongs
            marker_local - ndArray of shape (n_marker, 3)
                Array with cluster coordinates relative to the point
                in 0, 0, 0
        """
        self.point_name = point_settings['point_name']
        self.cluster_name = point_settings['cluster_name']
        self.cluster_marker_names = point_settings['cluster_marker_names']
        self.cluster_marker_indices = []
        self.marker_local = marker_local
        return


    def global_to_local(self, point_data, cluster_data):
        """
        Express the global cluster coordinates relative to some other
        global point to obtain local cluster coordinates wrt the point.
        
        When doing this, it is assumed that the cluster does NOT move wrt
        the point. There is no (local) base orientation for the cluster,
        so one needs to be extracted from the data.

        =INPUT=
            point_data - ndArray of shape (3,) or (n_frames, 3)
                Point data in global coordinates, relative to which the local
                cluster coordinates should be computed.
            cluster_data - ndArray of shape (n_frames, n_marker, 3)
                Cluster data in global coordinates.
            point_name - string
                Name of the point relative to which the cluster is defined.
                Note that the "same" cluster can be defined wrt multiple points.
                This requires multiple instances of this Cluster class, one
                for each point.
        =NOTES=
            TODO: What if a cluster or probe marker is invisible at all times,
            but you still have enough markers to do the reconstruction?
        """

        if len(point_data.shape) == 1:
            cluster_local = (cluster_data - point_data[np.newaxis, np.newaxis, :])
        elif len(point_data.shape) == 2:
            cluster_local = (cluster_data - point_data[:, np.newaxis, :])

        self.marker_local = np.nanmedian(cluster_local, axis=0)
        return


    def local_to_global(self, cluster_data):
        """
        Reconstruct the global position of the point, using its local
        information relative to the cluster as defined in self.marker_local.

        =INPUT=
            cluster_data - ndArray of shape (n_frames, n_markers, 3).
                Global data containing the markers of the cluster that 
                correspond to the markers in self.marker_local.
        =OUTPUT=
            marker_global - ndArray of shape (n_frame, 3)
                Reconstructed global position of the point).
        """
        marker_global = np.zeros((cluster_data.shape[0], 3), dtype='float')
        for i_frame in range(0, cluster_data.shape[0]):
            _, marker_global[i_frame, :] = r_svd(
                self.marker_local,
                cluster_data[i_frame, :, :],
                axis_dim=1)
        return marker_global


    def index_from_name(self, marker_data_labels):
        """
        Find the indices of the cluster marker names specified in point within
        the marker data of a trial, based on the marker_data_labels of that
        trial.

        =INPUT=
            marker_data_labels - list of strings
                Finds indices of strings that match with those in
                self.cluster_marker_names.
        """
        
        self.cluster_marker_indices = [
            marker_data_labels.index(label)
            for label in self.cluster_marker_names
            if label in marker_data_labels]
        return


class Points(object):
    """
    For all points specified in settings.json
    """

    def __init__(self):
        self.probe = None
        self.list = []
        return


    def add_probe(self, point_settings, probe_marker_local):
        self.probe = Point(point_settings, probe_marker_local)
        return


    def add_point(self, point_settings, marker_data, marker_data_labels):
        """
        From probe markers to probe tip (global point data)
        From probe tip to local cluster markers, relative to probe tip
        """
        
        self.probe.index_from_name(marker_data_labels)
        point_data = self.probe.local_to_global(
            marker_data[:, self.probe.cluster_marker_indices, :])
        
        point = Point(point_settings)
        point.index_from_name(marker_data_labels)
        point.global_to_local(
            point_data, marker_data[:, point.cluster_marker_indices, :])
        
        self.list.append(point)

        return


    def reconstruct(self, marker_data, marker_data_labels):
        """
        =INPUT=
            marker_data - ndArray of shape (n_frame, n_marker, 3)
            marker_data_labels - list of strings
        """

        reconstructed_data = np.zeros(
            (marker_data.shape[0], len(self.list), 3), dtype='float')
        reconstructed_labels = []

        for i_marker, point in enumerate(self.list):
            # TODO: add some warnings / catches here if clusters not found
            point.index_from_name(marker_data_labels)
            reconstructed_data[:, i_marker, :] = point.local_to_global(
                    marker_data[:, point.cluster_marker_indices, :])
            reconstructed_labels.append(point.point_name)

        return reconstructed_data, reconstructed_labels
