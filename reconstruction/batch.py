
import numpy as np
from py_readc3d import c3d
from reconstruction.point import Points


class BatchReconstruct(object):

    def __init__(self, settings):
        self.settings = settings
        self.reader = c3d.BasicReader()
        self.points = None
        self.data_per_trial = []
        return

    
    def run(self):
        """
        Reconstruct all points in all trials in the settings.
        """
        
        # Add probe
        self.points = Points()
        probe_marker_local = np.genfromtxt(self.settings['probe']['marker_local'],
            delimiter=',', dtype='float')
        self.points.add_probe(self.settings["probe"], probe_marker_local)

        # Add local point info based on probe measurements
        for pt in self.settings["points"]:
            
            # Load probe measurement
            data = self.reader.read(pt["probe_measurement_path"])
            marker_data = data.marker[:, :, 0:3]
            marker_data_labels = data.parameter_groups["point"]["parameters"]["labels"]["data"]

            # Determine local information of cluster markers wrt probe point
            self.points.add_point(pt, marker_data, marker_data_labels)

        # Go through all trials and reconstruct the points
        path_base = self.settings["trials"]["path_base"]
        for formats in self.settings["trials"]["path_formats"]:
            data = self.reader.read(path_base.format(*formats))
            marker_data = data.marker[:, :, 0:3]
            marker_data_labels = data.parameter_groups["point"]["parameters"]["labels"]["data"]
            point_data, point_data_label = self.points.reconstruct(marker_data, marker_data_labels)
            
            # New attribute in data object
            data.marker_reconstruct = point_data
            data.marker_reconstruct_label = point_data_label
            self.data_per_trial.append(data)

        return