"""
Rotation matrix tools
"""

import numpy as np
import glog as log


def r_svd(base1, base2, axis_dim=0, is_error=False):
    """
    Estimate a Euler rotation matrix between base1 and base2 in a
    least-squares way using singular-value decomposition.

    =INPUT=
        base1, base2 - ndArray
            Two dimensional array, of which one dimension is of size 3
        axis_dim - int
            Axis of base1, base2 that contains the (spatial) data dimensions
            (e.g. x, y, z).
        is_err - bool
            If true, supply the mean least-squares estimation error
    =OUTPUT=
        r_mat - ndArray
            The estimated rotation matrix from base1 to bsae2
        trans - scalar
            The estimated translation from base1 to base2
        error - scalar
            The root-mean square error associated with the estimate
    """

    if axis_dim != 0:
        base1 = base1.T
        base2 = base2.T
    if base1.shape[0] != 3 or base1.shape != base2.shape:
        log.error(f'Incorrect input dimensions: {base1.shape} vs {base2.shape}.')
        return None
    
    # Note that broadcasting occurs here...
    base1_mean = np.mean(base1, axis=1, keepdims=True)
    base2_mean = np.mean(base2, axis=1, keepdims=True)
    base1_zero = base1 - base1_mean
    base2_zero = base2 - base2_mean
    [u_arr, _, vh_arr] = np.linalg.svd(base2_zero @ base1_zero.T)
    
    # Rotation matrix
    r_mat = (u_arr * np.array([1, 1, np.linalg.det(u_arr @ vh_arr)])) @ vh_arr
    
    # Translation vector
    trans = base2_mean - r_mat @ base1_mean

    # RMS Error
    if is_error:
        error = ((base2 - (r_mat @ base1 + trans))**2).mean()**0.5
    if axis_dim:
        trans = trans.T
        
    if is_error:
        return r_mat, trans, error
    else:
        return r_mat, trans
