
# About settings.json
The settings.json file consists of 3 main parts:
* probe
* points
* trials
See below for more details.

## probe
Contains information about the probe. The probe tip can also be treated as a point. Most relevant is the marker_local path to the probe.csv file (see below). The order of the names in cluster_marker_names must correspond with the rows in probe.csv.

## points
List of dicts. Contains information about the name of each point, the name of the cluster the point belongs to, the marker names in that cluster, and the probe measurement file name. For measurement trials, it is also required to know what the cluster marker indices are. These are extracted for each trial based on the label names of the markers. It is therefore important that consistent marker naming is used. The cluster name is for reference purposes only. It does not influence the code execution.

## trials
The trials for which the probe points are to be reconstructed. The base path is formatted with the path formats. Use a list for each trial in path_formats (is unpacked upon formatting path_base).

# About probe.csv
CSV file, with comma as delimiter. Each row contains the x, y, z coordinates of an optical marker. They are local coordinates, RELATIVE to the probe tip. That is, the tip is in 0, 0, 0.

## Coordinates and LED locations
The rows correspond with the following marker locations:

SHAFT
CENTER
RIGHT
LEFT


(LEFT) (CENTER)(RIGHT)
O______0______O
       |
       |
       |
       O (SHAFT)
       |
       |
       V
       (TIP)
