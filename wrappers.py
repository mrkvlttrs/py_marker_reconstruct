"""
File for wrappers around the reader and converter
Requires python3.8 due shared_memory
"""

import numpy as np
from multiprocessing import shared_memory

from point import Cluster


def cluster_local_to_global(cluster, cluster_data, shm_idx, shm_name, shm_shape):
    """
    Wrapper around cluster.local_to_global, to have a target function for
    multiprocessing. A pre-created shared_memory variable in the parent
    process must be used to communicate to the parent.
    Requires python3.8 or higher.

    =INPUT=
        cluster - Cluster object with the .local_to_global method
        cluster_data - ndArray
            Data to feed to cluster.local_to_global
        shm_idx - int
            Index in dimension 1 of the shared memory point at which the data
            needs to be placed
        shm_name - string
        shm_shape - tuple
            Shape of the numpy array to create. Must match the shape of the
            array in the parent process.
    =NOTES=
        No output. Point results accessible through shared mem.
    """

    shm = shared_memory.SharedMemory(name=shm_name)
    point = np.ndarray(shm_shape, dtype='float', buffer=shm.buf)

    point_data = cluster.local_to_global(cluster_data)
    point[:, shm_idx, :] = point_data[:]

    # Clean up
    del point_data
    shm.close()

    return

